export const HTTP_CONFIG = {
    HOST_URL: 'http://localhost:8082/',
    AUTH: 'secure',
    // HOST_URL: '',
  };

export const LOCAL_STORAGE = {
  TOKEN: 'authToken',
  USER_DETAIL: 'userDetail'
}
 export const DURATION = {
   TEST_DURATION: 3600000
 }