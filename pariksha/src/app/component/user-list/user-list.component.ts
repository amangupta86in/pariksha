import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatDialog } from '@angular/material';

import { AddUserComponent } from '../add-user/add-user.component'
import { AddUserService } from '../../services/user.service';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  displayedColumns = ['firstName', 'lastName', 'email', 'role', 'action'];
  userList: any;
  searchUser: string;

  userRoleOption = {
      1: 'Admin',
      2: 'User'
  }

  constructor(
    private userService: AddUserService, 
    public dialog: MatDialog
  ) {
    this.getUserList();
  }

  ngOnInit() {
  }

  getUserList() {
    this.userService.getUserList().subscribe((response: any) => {
      this.userList = new MatTableDataSource(response.payload);
    });
  }

  addEditUserDialog(data) {
    let userDetail = Object.assign({}, data);
    const dialogRef = this.dialog.open(AddUserComponent,
      {
        width: '600px',
        data: userDetail
      });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      this.getUserList();
    });
  }

  applyFilter(filterValue: string) {
    this.userList.filter = filterValue.trim().toLowerCase();
  }

}