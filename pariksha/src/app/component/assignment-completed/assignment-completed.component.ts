import { Component, OnInit,Inject } from '@angular/core';
import {MatSnackBar, MatDatepicker, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';

@Component({
  selector: 'app-assignment-completed',
  templateUrl: './assignment-completed.component.html',
  styleUrls: ['./assignment-completed.component.css']
})
export class AssignmentCompletedComponent implements OnInit {

  constructor(
    public dialogRef: MatDialogRef<AssignmentCompletedComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) { }

  ngOnInit() {
  }

  onDialogClose(result): void {
    this.dialogRef.close(result);
  }
}
