import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AssignmentCompletedComponent } from './assignment-completed.component';

describe('AssignmentCompletedComponent', () => {
  let component: AssignmentCompletedComponent;
  let fixture: ComponentFixture<AssignmentCompletedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AssignmentCompletedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AssignmentCompletedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
