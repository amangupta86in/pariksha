import { Component, OnInit } from '@angular/core';
import { Validators, FormControl, FormGroup, ReactiveFormsModule, FormBuilder, FormsModule } from '@angular/forms';
import {Router} from '@angular/router';

//custome component/models/services
import { Logindetail } from '../../models/model';
import { SnackbarService } from '../../services/snackbar.service';
import { AddUserService } from '../../services/user.service';
import { LocalStorageService } from '../../services/local-storage.service';

import { LOCAL_STORAGE } from '../../constants/constants';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})



export class LoginComponent implements OnInit {
  user = {
    email: 'Admin@gmail.com',
    password: '123456'
  };
  result: any = {};
  loginForm: any;

  constructor(
    private router: Router,
    private userService: AddUserService,
    private snackbar: SnackbarService,
    private localStorageService: LocalStorageService
  ) { };

  ngOnInit() {
    let currentUrl = this.router.url;
    if(currentUrl == '/login') {
      this.logout();
    }

    this.loginForm = new FormGroup({
      email: new FormControl('', [
        Validators.required,
        Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/) 
      ]),
      password: new FormControl('', [
          Validators.minLength(6),
          Validators.required
      ]),
    });
  }

  submit() {
    console.log(this.user);

    this.userService.login(this.user).subscribe(response => {
      let message;
      if(response.success) {
        this.result = response.payload;
        console.log("response : ", response);
        this.localStorageService.setData(LOCAL_STORAGE.TOKEN, this.result.token);
        this.localStorageService.setData(LOCAL_STORAGE.USER_DETAIL, JSON.stringify(this.result));
        // this.router.navigate(['/home/userList']);
        let role = this.result.role || '';
        if(role.toLowerCase() == 'candidate') {
          this.router.navigate(['/assignment']);
        } else {
          this.router.navigate(['/home/userList']);
        }

      } else {
        message = 'Invalid credential.';
        this.snackbar.openSnackBar(message, 'Dismiss');
      }
    });
  }

  logout() {
    this.localStorageService.removeSelectedData(LOCAL_STORAGE.TOKEN);
    this.localStorageService.removeSelectedData(LOCAL_STORAGE.USER_DETAIL);
  }
}
