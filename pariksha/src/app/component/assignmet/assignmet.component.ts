import { Component, OnInit } from '@angular/core';
import { MatDialog } from '@angular/material';

import { QuestionService } from '../../services/question.service';
import { Question } from '../../models/model';
import { SnackbarService } from '../../services/snackbar.service';
import { LocalStorageService } from '../../services/local-storage.service';
import { LOCAL_STORAGE, DURATION } from '../../constants/constants';
import { CommonService } from '../../services/common.service';
import { AssignmentCompletedComponent } from '../assignment-completed/assignment-completed.component';

@Component({
  selector: 'app-assignmet',
  templateUrl: './assignmet.component.html',
  styleUrls: ['./assignmet.component.css']
})
export class AssignmetComponent implements OnInit {
  questionList: Array<Question>;
  isTestStart = false;
  answerList = {};
  result: {};
  testInfo: any; 
  userDetail: any;
  technologyMap: any = {};
  
  //timer varriable
  displayHourMinutTimer: any = '00:00:00';
  endTime: number = DURATION.TEST_DURATION;
  timer: any;

  constructor(
    private questionService: QuestionService,
    private snackbar: SnackbarService,
    private localStorageService: LocalStorageService,
    private commonService: CommonService,
    public dialog: MatDialog
  ) {
    this.userDetail = JSON.parse(this.localStorageService.getData(LOCAL_STORAGE.USER_DETAIL));
    this.technologyMap = this.commonService.getTechnologyMap();
    this.getQuestionList();
   }
  

  ngOnInit() {
    
  }

  getQuestionList() {
    let candidateDetail = { candidateId: this.userDetail.candidateId };
    this.questionService.getQuestionList(candidateDetail).subscribe((response: any) => {
      if(response.success) {
        this.testInfo = response.payload;
        this.questionList = response.payload.questionList;
      }
      
    });
  }

  onTabChange() {
    const dialogRef = this.dialog.open(AssignmentCompletedComponent,
      {
        width: '300px',
        data: {}
      });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
    });
  }

  assignmentTabChanged(event) {
    this.onTabChange();
    this.submitAssignment()
  }

  submitAssignment() {
    clearTimeout(this.timer);

    let totalMarks = 0;
    let testSummary = [];
    this.questionList.forEach((ques, index) => {
      let ansObj = {
        questionId: ques.questionId, 
        answer: '', 
        marks: 0,
        isAttempted: false
      }
      if(this.answerList.hasOwnProperty('ans-' + index)) {
        totalMarks += ques.complexity;
        ansObj.marks = ques.complexity;
        ansObj.answer = ques.answer;
        ansObj.isAttempted = true;
      }
      testSummary.push(ansObj)
    })
    console.log("this.userDetail.candidateId: ",this.userDetail.candidateId);
    let answerBody = {
      candidateId: this.userDetail.candidateId,
      totalMarks: totalMarks,
      answerDetails: testSummary,
    
    }

    this.questionService.submitTest(answerBody).subscribe(response => {
      console.log("result: ", response);
      this.result = response.payload;
      let message = '';
      if(response.success) {
        this.isTestStart = !this.isTestStart;
        this.answerList = {};
        // this.exitFullScreen();
        message = 'Assignment have been submitted successfully.'
      } else {
        message = 'Unable to save assignment.'
      }      
      this.snackbar.openSnackBar(message, 'Dismiss');
    });
  }

  startTest() {
    this.isTestStart = !this.isTestStart;
    this.startTimer();
  }

  // fullScreen() {
  //   this.isTestStart = !this.isTestStart;
  //   let elem =  document.getElementById('assignment-body');
  //   let methodToBeInvoked;
  //   if (elem.requestFullscreen) {
  //     methodToBeInvoked = elem.requestFullscreen();
  //   } else if (elem.webkitRequestFullscreen) {
  //     methodToBeInvoked = elem.webkitRequestFullscreen;
  //   } else if (elem['mozRequestFullScreen']) {
  //     methodToBeInvoked = elem['mozRequestFullScreen'];
  //   } else if (elem['msRequestFullscreen']) {
  //     methodToBeInvoked = elem['msRequestFullscreen'];
  //   }
    
  //   if(methodToBeInvoked) {
  //     methodToBeInvoked.call(elem);
  //   }
  //   this.startTimer();
  // }

  // exitFullScreen() {
  //   let elem =  document.getElementById('assignment-body');
  //   let methodToBeInvoked;
  //   if (document.exitFullscreen) {
  //     methodToBeInvoked = document.exitFullscreen();
  //   }
  //   else if (document['msExitFullscreen']) {
  //     methodToBeInvoked = document['msExitFullscreen'];
  //   }
  //   else if (document['mozCancelFullScreen']) {
  //     methodToBeInvoked = document['mozCancelFullScreen'];
  //   }
  //   else if (document.webkitExitFullscreen) {
  //     methodToBeInvoked = document.webkitExitFullscreen();
  //   }
      
  //   if(methodToBeInvoked) {
  //     methodToBeInvoked.call(document);
  //   }
  // }

  startTimer() {
    this.endTime = DURATION.TEST_DURATION;
    this.timer = setInterval(() => this.displayTimer(), 1000);
  }

  displayTimer() {

    this.endTime = this.endTime -1000;
    let dateDifference: any = this.endTime;

    if (dateDifference == 0) {
      // clearTimeout(this.timer);
      // this.isTestStart = !this.isTestStart;
      this.submitAssignment();
    }

    let totalMillisecsLeft = dateDifference;
    let hour = Math.floor(totalMillisecsLeft / (1000 * 60 * 60));
    totalMillisecsLeft = totalMillisecsLeft - hour * (1000 * 60 * 60);
    let minut = Math.floor(totalMillisecsLeft / (1000 * 60 ))
    totalMillisecsLeft = totalMillisecsLeft - minut * (1000 * 60);
    let second = Math.ceil(totalMillisecsLeft / 1000);
    
    this.displayHourMinutTimer = (hour > 9 ? hour : '0' + hour) + ':' +  (minut > 9 ? minut : '0' + minut) + ':' + (second > 9 ? second : '0' + second);
  }

}

