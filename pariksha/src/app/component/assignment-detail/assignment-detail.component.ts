import { Component, OnInit, Inject } from '@angular/core';
import { MatDatepicker, MatSnackBar, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { CandidateDetailService } from '../../services/candidatedetail.service';
import { LOCAL_STORAGE } from '../../constants/constants';
import { LocalStorageService } from '../../services/local-storage.service';

@Component({
  selector: 'app-assignment-detail',
  templateUrl: './assignment-detail.component.html',
  styleUrls: ['./assignment-detail.component.css']
})
export class AssignmentDetailComponent implements OnInit {

  assignmnetDetail: any;
  userDetail: any;
  answerDetails: any;
  displayedColumns = ['question', 'answerOption', 'marks'];

  constructor(
    private candidateDetailService: CandidateDetailService,
    public dialogRef: MatDialogRef<AssignmentDetailComponent>,
    private localStorageService: LocalStorageService,
    @Inject(MAT_DIALOG_DATA) public data
  ) {
    this.userDetail = JSON.parse(this.localStorageService.getData(LOCAL_STORAGE.USER_DETAIL));
    this.getAssignmentDetail();
   }

  ngOnInit() {
  }

  getAssignmentDetail() {
    this.candidateDetailService.getAssignmentDetail(this.userDetail.candidateId).subscribe(response => {
      if(response.success) {
        this.assignmnetDetail = response.payload;
        this.answerDetails = this.assignmnetDetail.answerDetails
      }
    });
  }

}
