import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, Inject } from '@angular/core';
import { Validators, FormControl, FormGroup, ReactiveFormsModule, FormBuilder, FormsModule } from '@angular/forms';
import { MatDatepicker, MatSnackBar, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { QuestionService } from '../../services/question.service';
import { CandidateDetailService } from '../../services/candidatedetail.service';
import { CandidateDetail } from '../../models/model';

import { SnackbarService } from '../../services/snackbar.service';
import { CommonService } from '../../services/common.service';

@Component({
  selector: 'app-create-assignment',
  templateUrl: './create-assignment.component.html',
  styleUrls: ['./create-assignment.component.css']
})
export class CreateAssignmentComponent implements OnInit {
  @ViewChild('elementToFocus') _input: ElementRef;
  assignmentForm: any;
  result: any;
  title: string = 'Create Assignment'

  technologyList: Array<Object> = [];

  date = new FormControl(new Date());
  
  assignmentObj: CandidateDetail;
  
  positionList: any = []
//temp value assignment
  experienceList: any;;

  constructor(
    private questionService: QuestionService, 
    private candidateDetailService: CandidateDetailService,
    private snackbar: SnackbarService,
    private commonService: CommonService,
    public dialogRef: MatDialogRef<CreateAssignmentComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) { 
    this.addValidation();
    this.technologyList = this.commonService.getTechnologyList();
    this.positionList = this.commonService.getPositionList();
    this.experienceList = this.commonService.getExperienceList();
  }

  ngOnInit() {
    this.assignmentObj = {
      firstName: '',
      lastName: '',
      email: '',
      mobileNo: '',
      technologies: {
        primaryskill: [],
        secondaryskill: [],
      },
      validTill: this.date.value,
      profile: ''
    }

    if(Object.keys(this.data).length) {
      this.assignmentObj = this.data;
      this.title = 'Edit Candidate';
    }
  }

  submit() {
    let assignmentDetail = Object.assign({}, this.assignmentObj);
    assignmentDetail['isValid'] = true;
    assignmentDetail['score'] = 'Not Attempted';
    assignmentDetail['role'] = 'candidate';
    // console.log("assignmentObj: ",assignmentDetail);

    this.candidateDetailService.createAssignment(assignmentDetail).subscribe(response => {
      let message;
      if(response.success) {
        this.result = response.payload;
        message = 'You have created assignment successfully.';
        this.setFormPristine();
        this.onDialogClose(this.result);
      } else {
        message = 'Unable to created assignment.'
      }
      this.snackbar.openSnackBar(message, 'Dismiss');
      
    });
  }

  setFormPristine() {
    this.assignmentObj = {
      firstName: '',
      lastName: '',
      email: '',
      mobileNo: '',
      technologies: {
        primaryskill: [],
        secondaryskill: [],
      },
      validTill: this.date.value,
      profile: ''
    }
    this.assignmentForm.reset();
    Object.keys(this.assignmentForm.controls).forEach(key => {
      this.assignmentForm.controls[key].setErrors(null)
    });    
  }

  // getTechnologyList() {
  //   this.questionService.getTechnologyList().subscribe(response => {
  //     this.technologyList = response.payload;
  //   });
  // }
  
  getErrorMessage(prop, length) {
    return this.commonService.getErrorMessage(this.assignmentForm, prop, length);
  }

  addValidation() {
    this.assignmentForm = new FormGroup({
      firstName: new FormControl('', [
          Validators.required
      ]),
      lastName: new FormControl('', [
        Validators.required
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/) 
      ]),
      mobileNo: new FormControl('', [
        Validators.required,
        Validators.pattern(/[0-9]{10}/),
        Validators.minLength(10),
        Validators.maxLength(10)
      ]),
      // profile: new FormControl('', [
      //   Validators.required
      // ]),
      primerySkill: new FormControl('', [
        Validators.required
      ]),
      secondrySkill: new FormControl('', [
        // Validators.required
      ]),
      position: new FormControl('', [
        Validators.required
      ]),
      experience: new FormControl('', [
        Validators.required
      ]),
      validTill: new FormControl('', [
        Validators.required
      ])
    });
  }

  getOptionStatus(type, value) {
    if(type == 'primary') {
      return (this.assignmentObj.technologies.secondaryskill && this.assignmentObj.technologies.secondaryskill.indexOf(value) != -1);
    } else {
      return (this.assignmentObj.technologies.primaryskill && this.assignmentObj.technologies.primaryskill.indexOf(value) != -1);
    }
  }

  _openCalendar(picker: MatDatepicker<Date>) {
    picker.open();
    setTimeout(() => this._input.nativeElement.focus());
  }

  _closeCalendar(e) {
    setTimeout(() => this._input.nativeElement.blur());
  }
  onDialogClose(result): void {
    this.dialogRef.close(result);
  }
}
