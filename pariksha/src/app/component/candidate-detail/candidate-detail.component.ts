import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatDialog } from '@angular/material';

import { CandidateDetailService } from '../../services/candidatedetail.service';
import { QuestionService } from '../../services/question.service';

import { CreateAssignmentComponent } from '../create-assignment/create-assignment.component';
import { AssignmentDetailComponent } from '../assignment-detail/assignment-detail.component';
import { CommonService } from '../../services/common.service';

@Component({
  selector: 'app-candidate-detail',
  templateUrl: './candidate-detail.component.html',
  styleUrls: ['./candidate-detail.component.css']
})
export class CandidateDetailComponent implements OnInit {

  displayedColumns = ['firstName', 'lastName', 'email', 'mobileNo', 'position', 'technologies', 'score', 'action'];
  candidateList: any;
  technologyList: Array<Object>;
  searchUser: string;
  positionMap: any;
  technologyMap: any = {};

  constructor(
    private candidateDetailService: CandidateDetailService,
    private questionService: QuestionService,
    public dialog: MatDialog,
    private commonService: CommonService
  ) { 
    this.getCandidateList();
    this.positionMap = this.commonService.getPositionMap();
    this.technologyMap = this.commonService.getTechnologyMap();
    this.technologyList = this.commonService.getTechnologyList();
  }

  ngOnInit() {
  }

  getCandidateList() {
    this.candidateDetailService.getCandidateList().subscribe((response: any) => {
      this.candidateList = new MatTableDataSource(response.payload);
    });
  }

  candidateDialog(data, dialogType) {
    let detail = Object.assign({}, data);
    let component: any = dialogType == 'createAssignment' ? CreateAssignmentComponent : AssignmentDetailComponent
    const dialogRef = this.dialog.open(component,
      {
        width: '600px',
        data: detail
      });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      this.getCandidateList();
    });
  }

  deleteCandidate(data) {
    let restBody = {candidateId: data.candidateId};
    this.candidateDetailService.deleteCandidate(restBody).subscribe((response: any) => {
      this.getCandidateList();
    });
  }

  applyFilter(filterValue: string) {
    this.candidateList.filter = filterValue.trim().toLowerCase();
  }
}