import { Component, OnInit } from '@angular/core';
import { MatTableDataSource, MatDialog } from '@angular/material';

import { AddQuestionComponent } from '../add-question/add-question.component'
import { QuestionService } from '../../services/question.service';
import { SnackbarService } from '../../services/snackbar.service';
import { CommonService } from '../../services/common.service';

@Component({
  selector: 'app-question-list',
  templateUrl: './question-list.component.html',
  styleUrls: ['./question-list.component.css']
})
export class QuestionListComponent implements OnInit {
  displayedColumns = ['question', 'answerOption', 'answer', 'technologyId', 'complexity', 'action'];
  questionList: any;
  searchQuestion: string;

  technologyMap: any = {};

  techOption = [{technology: 'Angular', technologyId: 1}, {technology: 'Java', technologyId: 2}]


  constructor(
    private questionService: QuestionService, 
    public dialog: MatDialog,
    private snackbar: SnackbarService,
    private commonService: CommonService
  ) {
    this.getQuestionList();
    this.technologyMap = this.commonService.getTechnologyMap();
   }

  ngOnInit() {
  }

  getQuestionList() {
    this.questionService.getAllQuestionList().subscribe((response: any) => {
      this.questionList = new MatTableDataSource(response.payload);
    });
  }

  deleteQuestion (question) {
    let questionObj = {questionId: question.questionId};
    this.questionService.deleteQuestion(questionObj).subscribe((response: any) => {
      let message = '';
      if(response.success) {
        message = 'Question deleted successfully.';
        this.getQuestionList();
      } else {
        message = 'Unable to delete Question.';
      }
      this.snackbar.openSnackBar(message, 'Dismiss');
    });
  }

  addEditQuestionDialog(data) {
    let questiondetail = Object.assign({}, data);
    const dialogRef = this.dialog.open(AddQuestionComponent,
      {
        width: '600px',
        data: questiondetail
      });

    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      this.getQuestionList();
    });
  }

  applyFilter(filterValue: string) {
    this.questionList.filter = filterValue.trim().toLowerCase();
  }

  extractData(data) {

    let csvData = data;
    let allTextLines = csvData.split(/\r\n|\n/);
    let headers = allTextLines[0].split(',');
    let lines = [];

    for ( let i = 1; i < allTextLines.length; i++) {
        // split content based on comma
        let data = allTextLines[i].split(',');
        if (data.length == headers.length) {
          let techId = Object.keys(this.technologyMap).find(key => this.technologyMap[key] === data[headers.indexOf('Technology')]);
          let questionObj = {
            technologyId: techId,
            question: data[headers.indexOf('Question')],
            complexity: data[headers.indexOf('Complexity')],
            answer: data[headers.indexOf('Answer')],
            answerOption: {
              A: data[headers.indexOf('OptionA')],
              B: data[headers.indexOf('OptionB')],
              C: data[headers.indexOf('OptionC')],
              D: data[headers.indexOf('OptionD')]
            }
          }
          lines.push(questionObj);
        }
    }
    if(lines.length) {
      // API call to upload question list
      this.uploadAllQuestion(lines);
    } else {
      console.log('Nothing to upload.');
    }
  }

  uploadCSV(evt) {
    let files = evt.target.files;
    let file = files[0];
    let reader = new FileReader();
    reader.readAsText(file);
    reader.onload = (event: any) => {
      let csv = event.target.result; // Content of CSV file
      this.extractData(csv);
    }
  }

  uploadAllQuestion(questions) {
    this.questionService.addQuestion(questions).subscribe(response => {
      let message = '';
      if(response.success) {
        message = 'CSV questions uploaded successfully.';
        this.getQuestionList();
      } else {
        message = 'Unable to save Question.'
      }
      this.snackbar.openSnackBar(message, 'Dismiss');      
    });
  }
}
