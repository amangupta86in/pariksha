import { Component, OnInit,Inject } from '@angular/core';
import { Validators, FormControl, FormGroup, ReactiveFormsModule, FormBuilder, FormsModule } from '@angular/forms';
import {MatSnackBar, MatDatepicker, MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';


import { QuestionService } from '../../services/question.service';
import { Question } from '../../models/model';
import { SnackbarService } from '../../services/snackbar.service';
import { CommonService } from '../../services/common.service';
// import { AddQuestionComponent } from '../add-question/add-question.component'

@Component({
  selector: 'app-add-question',
  templateUrl: './add-question.component.html',
  styleUrls: ['./add-question.component.css']
})
export class AddQuestionComponent implements OnInit {

  result: Array<Object>;
  technologyList: Array<Object>;
  complexity: Array<Object>;
  questinForm: any;
  techMap: Object = {};

  title = 'Add Question';
  questionObj: Question = {
    technologyId: 0,
    question: '',
    complexity: 1,
    answer: '',
    answerOption: {
      A: '',
      B: '',
      C: '',
      D: ''
    }
  };

  constructor(
    private questionService: QuestionService,
    private snackbar: SnackbarService,
    private commonservice: CommonService,
    public dialogRef: MatDialogRef<AddQuestionComponent>,
    @Inject(MAT_DIALOG_DATA) public data
  ) { 
      this.addValidation();
      this.complexity = this.commonservice.getQuestioncoplexity();
      this.technologyList = this.commonservice.getTechnologyList();
      this.techMap = this.commonservice.getTechnologyMap();
   }

  ngOnInit() {
    if(Object.keys(this.data).length) {
      this.questionObj = this.data;
      this.title = 'Edit Question';
    }
  }

  submit() {
    var self = this;

    this.questionService.addQuestion(this.questionObj).subscribe(response => {
      this.result = response.payload;
      let message = '';
      if(response.success) {
        message = this.questionObj.questionId ? 'You have updated ' + this.techMap[this.questionObj.technologyId] + ' question successfully.' : 'You have added ' + this.techMap[this.questionObj.technologyId] + ' question successfully.';
        this.setFormPristine();
        this.onDialogClose(this.result);
      } else {
        message = 'Unable to save Question.'
      }
      this.snackbar.openSnackBar(message, 'Dismiss');      
    });
  }

  setFormPristine() {
    this.questinForm.reset();
    Object.keys(this.questinForm.controls).forEach(key => {
      this.questinForm.controls[key].setErrors(null)
    });
  }

  getErrorMessage(prop, length) {
    return this.commonservice.getErrorMessage(this.questinForm, prop, length);
  }

  onDialogClose(result): void {
    this.dialogRef.close(result);
  }

  addValidation() {
    this.questinForm = new FormGroup({
      question: new FormControl('', [
          Validators.required
      ]),
      answerGroup: new FormControl('', [
        Validators.required
      ]),
      optionA: new FormControl('', [
        Validators.required
      ]),
      optionB: new FormControl('', [
        Validators.required
      ]),
      optionC: new FormControl('', [
        Validators.required
      ]),
      optionD: new FormControl('', [
        Validators.required
      ]),
      complexity: new FormControl('', [
        Validators.required
      ]),
      technologyId: new FormControl('', [
        Validators.required
      ]),
    });
  }
}
