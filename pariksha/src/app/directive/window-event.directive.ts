import { Directive, HostListener, Output, EventEmitter  } from '@angular/core';

@Directive({
  selector: '[appWindowEvent]'
})
export class WindowEventDirective {
  @Output() appWindowEvent = new EventEmitter();
  constructor() { }

  @HostListener('window:focus', ['$event'])
    onFocus(event: any): void {
        console.log("on focus!")
    }

  @HostListener('window:blur', ['$event'])
    onBlur(event: any): void {
        console.log("on blur!")
        // this.appWindowEvent.emit();
    }
  @HostListener('window:pageshow', ['$event'])
    onPageShow(event):void {
      console.log('page show!');
    }
  @HostListener('window:beforeunload') onBeforeUnload() {
      console.log('page hide!');
    }

}
