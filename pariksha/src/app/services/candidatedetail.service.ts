import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { HTTP_CONFIG } from '../constants/constants';
import { CandidateDetail } from '../models/model';

@Injectable({
  providedIn: 'root'
})
export class CandidateDetailService {
  private baseUrl = HTTP_CONFIG.HOST_URL;
  private secure = HTTP_CONFIG.AUTH;

  constructor(private http: HttpClient) { }

  getCandidateList(): Observable<ServerResponse> {
    return this.http.get<ServerResponse>(
      this.baseUrl + 'candidateDetail/'+ this.secure +'/getCandidateList');
  }

  createAssignment(assignmentDetail: CandidateDetail): Observable<ServerResponse> {
    if(assignmentDetail.candidateId) {
      return this.http.post<ServerResponse>(
        this.baseUrl + 'candidateDetail/'+ this.secure + '/updateCandidate', assignmentDetail);
    } else {
      return this.http.post<ServerResponse>(
        this.baseUrl + 'candidateDetail/'+ this.secure + '/createAssignment', assignmentDetail);
    }
  }

  deleteCandidate(restBody: Object): Observable<ServerResponse> {
    return this.http.post<ServerResponse>(
      this.baseUrl + 'candidateDetail/'+ this.secure +'/deleteCandidate', restBody);
  }

  getPositionList(): Observable<ServerResponse> {
    return this.http.get<ServerResponse>(
      this.baseUrl + 'candidateDetail/'+ this.secure +'/getPositionList');
  }

  getExperienceList(): Observable<ServerResponse> {
    return this.http.get<ServerResponse>(
      this.baseUrl + 'candidateDetail/'+ this.secure +'/getExperienceList');
  }

  getAssignmentDetail(candidateId): Observable<ServerResponse> {
    return this.http.get<ServerResponse>(
      this.baseUrl + 'candidateDetail/'+ this.secure +'/getAssignmentDetail?candidateId='+ candidateId);
  }

}

export declare interface ServerResponse {
  success: boolean;
  payload: Array<Object>
}