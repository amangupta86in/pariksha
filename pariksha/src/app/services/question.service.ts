import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { HTTP_CONFIG } from '../constants/constants';
import {Question} from '../models/model';

@Injectable({
  providedIn: 'root'
})
export class QuestionService {

  private baseUrl = HTTP_CONFIG.HOST_URL;
  private secure = HTTP_CONFIG.AUTH;

  constructor(private http: HttpClient) { }

  addQuestion(questionObj: Question): Observable<ServerResponse> {
      if(questionObj.questionId) {
        return this.http.post<ServerResponse>(
          this.baseUrl + 'question/'+ this.secure +'/editQuestion', questionObj);
      } else {
        return this.http.post<ServerResponse>(
          this.baseUrl + 'question/'+ this.secure +'/addQuestion', questionObj);
      }
  }

  deleteQuestion (questionObj): Observable<ServerResponse> {
    return this.http.post<ServerResponse>(
      this.baseUrl + 'question/'+ this.secure +'/deleteQuestion', questionObj);
  }

  getComplexity(): Observable<ServerResponse> {
    return this.http.get<ServerResponse>(
      this.baseUrl + 'question/'+ this.secure +'/getQuestionComplexity');
  }

  getTechnologyList(): Observable<ServerResponse> {
    return this.http.get<ServerResponse>(
      this.baseUrl + 'question/'+ this.secure +'/getTechnologyList');
  }

  getQuestionList(userDetail): Observable<ServerResponse> {
    return this.http.get<ServerResponse>(
      this.baseUrl + 'assignement/'+ this.secure +'/getQuestionList?candidateId=' + userDetail.candidateId);
  }
  getAllQuestionList(): Observable<ServerResponse> {
    return this.http.get<ServerResponse>(
      this.baseUrl + 'question/'+ this.secure +'/getQuestionList');
  }
  submitTest(answerBody): Observable<ServerResponse>{
    return this.http.post<ServerResponse>(
      this.baseUrl + 'assignement/'+ this.secure +'/submitTest', answerBody);
  }
}

export declare interface ServerResponse {
  success: boolean;
  payload: Array<Object>
}
