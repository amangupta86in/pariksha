import { Injectable } from '@angular/core';

import { QuestionService } from '../services/question.service';

import { CandidateDetailService } from '../services/candidatedetail.service'

@Injectable({
  providedIn: 'root'
})
export class CommonService {
  technologyList: Array<Object> = [];
  technologyMap: Object = {};
  complexityList: Array<Object> = [];
  userRoleList: Array<Object> = [];
  userRoleMap: Object = {};
  positionList: any;
  experienceList: any;
  positionMap: any = {};

  constructor(
    private questionService: QuestionService, 
    private candidateDetailService: CandidateDetailService
  ) { 
    this.setQuestionComplexity();
    this.setTechnologyList();
    this.setPositionList();
    this.setExperienceList();
  }

  getErrorMessage(form, prop, length) {
    let errMessage = 'Required';
    if(form.controls[prop].hasError('required')) {
      errMessage = 'Required';
    } else if (form.controls[prop].hasError('pattern')) {
      errMessage = 'Not a valid ' + prop;
    } else if (form.controls[prop].hasError('minlength')) {
      errMessage = 'Required length is at least ' + length + ' characters';
    }
    return errMessage;
  }

  setTechnologyList() {
    this.questionService.getTechnologyList().subscribe(response => {
      if(response.success) {
        this.technologyList = response.payload;
        this.technologyList.forEach((value, key) => {
          this.technologyMap[value['technologyId']] = value['technology'];
        })
      }
    });
  }

  setPositionList() {
    this.candidateDetailService.getPositionList().subscribe(response => {
      if(response.success) {
        this.positionList = response.payload;
        this.positionList.forEach((positionObj, index) => {
          this.positionMap[positionObj['positionId']] = positionObj['position'];
        })
      }
    });
  }

  setExperienceList() {
    this.candidateDetailService.getExperienceList().subscribe(response => {
      if(response.success) {
        this.experienceList = response.payload;
      }
    });
  }

  setQuestionComplexity() {
    this.questionService.getComplexity().subscribe(response => {
      if(response.success) {
        this.complexityList = response.payload;
      }
    });
  }

  getQuestioncoplexity() {
    return this.complexityList;
  }

  getTechnologyList() {
    return this.technologyList;
  }

  getTechnologyMap() {
    return this.technologyMap;
  }

  getPositionList() {
    return this.positionList;
  }

  getPositionMap() {
    return this.positionMap;
  }

  getExperienceList() {
    return this.experienceList;
  }

}
