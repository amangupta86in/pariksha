import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

import { HTTP_CONFIG } from '../constants/constants';

import { UserDetail, CandidateDetail, Logindetail } from '../models/model';

@Injectable({
  providedIn: 'root'
})
export class AddUserService {
  private baseUrl = HTTP_CONFIG.HOST_URL;
  private secure = HTTP_CONFIG.AUTH;

  constructor(private http: HttpClient) { }

  addUser(userDetail: UserDetail): Observable<ServerResponse> {
    if(userDetail.userId) {
      return this.http.post<ServerResponse>(
        this.baseUrl + 'user/'+ this.secure +'/updateUser', userDetail);
    } else {
      return this.http.post<ServerResponse>(
        this.baseUrl + 'user/'+ this.secure + '/addUser', userDetail);
    }
  }

  getUserList(): Observable<ServerResponse> {
    return this.http.get<ServerResponse>(
      this.baseUrl + 'user/'+ this.secure + '/getUserList');
  }

  getUserRole(): Observable<ServerResponse> {
    return this.http.get<ServerResponse>(
      this.baseUrl + 'user/'+ this.secure + '/getUserRole'
    )
  }

  login(loginObj: Logindetail): Observable<ServerResponse> {
    return this.http.post<ServerResponse>(
      this.baseUrl + 'user/login', loginObj
    )
  }
}

export declare interface ServerResponse {
  success: boolean;
  payload: Array<Object>
}