import { NgModule } from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import { LoginComponent } from '../component/login/login.component';
import { HomeComponent } from '../component/home/home.component';
import { AuthGuardService } from '../services/auth-guard.service';
import { AssignmetComponent } from '../component/assignmet/assignmet.component';
import { QuestionListComponent } from '../component/question-list/question-list.component';
import { CreateAssignmentComponent } from '../component/create-assignment/create-assignment.component';
import { UserListComponent } from '../component/user-list/user-list.component';
import { CandidateDetailComponent } from '../component/candidate-detail/candidate-detail.component';
import { PendingAssignmentsComponent } from '../component/pending-assignments/pending-assignments.component';


const appRoutes: Routes = [
  { 
    path: '', 
    redirectTo: '/login', 
    pathMatch: 'full' 
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'assignment',
    component: AssignmetComponent
  },
  {
    path: 'home',
    component: HomeComponent,
    children: [
      { path: '', redirectTo: '/userList', pathMatch: 'full' }, 
      { path: 'questionList', component: QuestionListComponent }, 
      { path: 'createAssignment', component: CreateAssignmentComponent }, 
      { path: 'userList', component: UserListComponent },
      { path: 'candidateDetail', component: CandidateDetailComponent },
      { path: 'pendingAssignment', component: PendingAssignmentsComponent },
    ]
  }
]
@NgModule({
  exports: [RouterModule],
  imports: [
    RouterModule.forRoot(appRoutes),
  ],
  declarations: []
})

export class AppRoutingModule { }
