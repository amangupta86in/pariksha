import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule, HTTP_INTERCEPTORS }   from '@angular/common/http';
import { 
  MatButtonModule, 
  MatCardModule, 
  MatFormFieldModule, 
  MatInputModule, 
  MatToolbarModule,
  MatIconModule,
  MatTableModule,
  MatSidenavModule,
  MatListModule,
  MatRadioModule,
  MatSelectModule,
  MatSnackBarModule,
  MatStepperModule,
  MatCheckboxModule,
  MatDatepickerModule,
  MatProgressSpinnerModule,
  MatNativeDateModule,
  MatDialogModule
} from '@angular/material';
import {MatTooltipModule} from '@angular/material/tooltip';


// Routing module
import { AppRoutingModule } from './route/app-routing.module';

//custome component/services
import { AppComponent } from './app.component';
import { LoginComponent } from './component/login/login.component';
import { HomeComponent } from './component/home/home.component';
import { AddQuestionComponent } from './component/add-question/add-question.component';
import { AssignmetComponent } from './component/assignmet/assignmet.component';
import { DocumentEventsDirective } from './directive/document-events.directive';
import { FullScreenEventDirective } from './directive/full-screen-event.directive';
import { CreateAssignmentComponent } from './component/create-assignment/create-assignment.component';
import { AddUserComponent } from './component/add-user/add-user.component';
import { UserListComponent } from './component/user-list/user-list.component';
import { CandidateDetailComponent } from './component/candidate-detail/candidate-detail.component';
import { WindowEventDirective } from './directive/window-event.directive';
import { AuthIntercepterService } from './services/auth-intercepter.service';
import { QuestionListComponent } from './component/question-list/question-list.component';
import { PendingAssignmentsComponent } from './component/pending-assignments/pending-assignments.component';
import { AssignmentDetailComponent } from './component/assignment-detail/assignment-detail.component';
import { AssignmentCompletedComponent } from './component/assignment-completed/assignment-completed.component'

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    HomeComponent,
    AddQuestionComponent,
    AssignmetComponent,
    DocumentEventsDirective,
    FullScreenEventDirective,
    CreateAssignmentComponent,
    AddUserComponent,
    UserListComponent,
    CandidateDetailComponent,
    WindowEventDirective,
    QuestionListComponent,
    PendingAssignmentsComponent,
    AssignmentDetailComponent,
    AssignmentCompletedComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,
    HttpClientModule,

    BrowserAnimationsModule,
    MatCardModule,
    MatFormFieldModule,
    MatInputModule,
    MatButtonModule,    
    MatToolbarModule,
    MatIconModule,
    MatTableModule,
    MatSidenavModule,
    MatListModule,
    MatRadioModule,
    MatSelectModule,
    MatSnackBarModule,
    MatStepperModule,
    MatCheckboxModule,
    MatDatepickerModule,
    MatProgressSpinnerModule,
    MatNativeDateModule,
    MatDialogModule,
    MatTooltipModule
  ],
  entryComponents: [
    UserListComponent, 
    AddUserComponent,
    AddQuestionComponent,
    AssignmentDetailComponent,
    AssignmentCompletedComponent
  ],
  providers: [
    {
      provide : HTTP_INTERCEPTORS,
      useClass : AuthIntercepterService,
      multi : true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
