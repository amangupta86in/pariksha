const express = require('express');
const bodyParser = require('body-parser');
const jwt = require('jsonwebtoken');
const path = require('path');

const config = require('./config');
const customResponses = require('./middlewares/customResponses');

const app = express();
const port = process.env.PORT || config.port;
const ENV = process.env.NODE_ENV || config.env;

app.set('env', ENV);

app.use(bodyParser.json());
app.use('/', express.static(path.join(__dirname, 'client')));

app.use(customResponses);

// This route will check if the api call should be authenticated.
app.use((req, res, next) => {
  const originalUrl = req.originalUrl;
  if ((originalUrl.indexOf('secure') != -1) && req.method !== 'OPTIONS')  {
    authenticateUser(req,res, next);
  } else {
    next();
  }
});

function authenticateUser(req, res,next) {
  var token = req.headers['x-access-token'];
  if (!token){
    return  res.unauthorized();
  } 
  jwt.verify(token, config.TOKEN_SECRET, (err, decoded) => {
    if (err) {
      return serverError();
      }
    next();
  });
}

// cross origin headers
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
  res.header(
      'Access-Control-Allow-Headers',
      'Origin, X-Requested-With, Content-Type, Accept, Authorization, x-access-token');
  next();
});




require('./config/mongoose')(app);
require('./app')(app);

app.use((req, res) => {
  res.notFound();
});

app.use((err, req, res, next) => {
  next(err);
});

// Don't remove next !!!!
app.use((err, req, res, next) => {  // eslint-disable-line no-unused-vars
  res.status(503).json({
    success: false,
    error: 'server_error',
  });
});

app.listen(port, () => {
  console.log(`Listening on port ${port}`);
});
