const config = require( "./index" );
const mongoose = require( "mongoose" );

// const mongoUrl = 'mongodb://localhost:27017/pariksha';
const mongoUrl = 'mongodb://' + config.user + ':' + config.password + '@' + config.dbUrl + '/' + config.dbName;
console.log("mongoUrl:", mongoUrl);

module.exports = function( app ) {
    mongoose.connect( mongoUrl, { useNewUrlParser: true } );
    mongoose.Promise = global.Promise;

    process.on( "SIGINT", cleanup );
    process.on( "SIGTERM", cleanup );
    process.on( "SIGHUP", cleanup );

    if ( app ) {
        app.set( "mongoose", mongoose );
    }
};

function cleanup( ) {
    mongoose.connection.close( function( ) {
        process.exit( 0 );
    } );
}
