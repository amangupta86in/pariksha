const questionFilter = {
    '1': { primary: 3, secondary: 2 },
    '2': { primary: 4, secondary: 3 },
    '3': { primary: 5, secondary: 3 },
    '4': { primary: 5, secondary: 4 }
}

module.exports = questionFilter;