const services = require( "./services" );
const questionServices = require( "../question/services" );

exports.getCandidateList = async (req, res) => {
    try {
        const candidateList = await services.getCandidatesList();
        res.success(candidateList);
    } catch(err) {
        res.send(err);
    }
};

exports.getCandidateDetail = async (req, res) => {
    try {
        const candidateDetail = await services.getCandidateDetail(req.query);
        res.success(candidateDetail);
    } catch(err) {
        res.send(err);
    }
};


exports.createAssignment = async ( req, res ) => {
    try {
        const getResult = await services.createAssignment( req.body );
        res.success( getResult );
    } catch ( err ) {
        res.send( err );
    }
};

exports.updateCandidate = async (req, res) => {
    try {
      const getResult = await services.updateCandidate(req.body);
      res.success(getResult);
    } catch (err) {
      res.send(err);
    }
};

exports.deleteCandidate = async (req, res) => {
    try {
      const deleteCandidate = await services.deleteCandidate(req.body);
      res.success(deleteCandidate);
    } catch (err) {
        res.send(err);
      }
};

exports.cancelAssignment = async (req, res) => {
    try {
        const cancelAssignment = await services.cancelAssignment(req.body); 
        res.success(cancelAssignment);
    } catch (err) {
        res.send(err);
    }
};

exports.getPositionList = async ( req, res ) => {
    try {
        const positionList = await services.getPositionList(req.body); 
        res.success(positionList);
    } catch (err) {
        res.send(err);
    }
}

exports.getExperienceList = async ( req, res ) => {
    try {
        const experienceList = await services.getExperienceList(req.body); 
        res.success(experienceList);
    } catch (err) {
        res.send(err);
    }
}

exports.getAssignmentDetail = async ( req, res ) => {
    try {
        //fetch assignment detail
        let assignmentDetail = await services.getAssignmentDetail(req.query);
        //fetch all question id from assignment detail list
        const questionIds = getAllQuestionIds(assignmentDetail.answerDetails);
        //fetch all question list with ids array
        const questionList = await questionServices.getQuestionList(questionIds);
        //mapping question to corresponding assignment object
        mapQuestiondetail(questionList, assignmentDetail);
        res.success(assignmentDetail);
    } catch (err) {
        res.send(err);
    }
}

function getAllQuestionIds(questionList) {
    const questionIds = [];
    for (const question of questionList) {
        questionIds.push(question.questionId);
    }
    return questionIds;
}

function mapQuestiondetail(questionList, assignmentDetail) {
    assignmentDetail.answerDetails.forEach((answerObj, index) =>  {
        answerObj['question'] = (questionList[index] && questionList[index].question) ? questionList[index].question : 'NA';
        answerObj['answerOption'] = answerObj.answer ? questionList[index].answerOption[answerObj.answer] : 'NA';
    })
}