require( "../../models/candidate_model" );
const controller = require( "./controller" );
const express = require( "express" );
const router = express.Router( );

router.get( "/secure/getCandidateList", controller.getCandidateList );
router.post( "/secure/createAssignment", controller.createAssignment );
router.post( "/secure/updateCandidate", controller.updateCandidate );
router.post( "/secure/deleteCandidate", controller.deleteCandidate );

router.get( "/secure/getPositionList", controller.getPositionList );
router.get( "/secure/getExperienceList", controller.getExperienceList );
router.get( "/secure/getAssignmentDetail", controller.getAssignmentDetail );

module.exports = router;
