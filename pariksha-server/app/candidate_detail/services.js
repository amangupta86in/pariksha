const mongoose = require( "mongoose" );

const CandidateDetail = mongoose.model( "CandidateDetail" );
const ProfileMaster = mongoose.model( "ProfileMaster" );
const ExperienceMaster = mongoose.model( "ExperienceMaster" );
const TestDetails = mongoose.model( "TestDetails" );

const getCandidatesList = () => {
    return CandidateDetail.find();
};
const getCandidateDetail = (queryParam) => {
    const query = {candidateId: queryParam.candidateId}
    return CandidateDetail.find(query);
};

const createAssignment = ( data ) => {
    const candidateDetail = new CandidateDetail( data );
    return candidateDetail.save();
};
const updateCandidate = (data) => {
    const query = {candidateId: data.candidateId};
    return CandidateDetail.findOneAndUpdate(query, {$set: data}, {new: true});
};

const deleteCandidate = (candidateInfo) => {
    return CandidateDetail.findOneAndRemove({'candidateId': candidateInfo.candidateId});
};

const getPositionList = () => {
    return ProfileMaster.find();
};
const getExperienceList = () => {
    return ExperienceMaster.find();
};

const getAssignmentDetail = (param) => {
    const query = {'candidateId': param.candidateId};
    return TestDetails.findOne(query);
}


module.exports = {
    getCandidatesList,
    createAssignment,
    updateCandidate,
    deleteCandidate,
    getPositionList,
    getExperienceList,
    getCandidateDetail,
    getAssignmentDetail
};
