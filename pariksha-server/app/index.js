const questionRouter = require( "./question/router" );
const assignmentRouter = require( "./assignment/router" );
const userRouter = require( "./user/router" );
const candidateDetailRouter = require('./candidate_detail/router');

module.exports = ( app ) => {
    app.use( "/question", questionRouter );
    app.use( "/assignement", assignmentRouter );
    app.use( "/user", userRouter );
    app.use( "/candidateDetail", candidateDetailRouter );
};
