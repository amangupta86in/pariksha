
const mongoose = require( "mongoose" );

const QuestionMaster = mongoose.model( "QuestionMaster" );
const TechnologyMaster = mongoose.model( "TechnologyMaster" );
const QuestionComplexityMaster = mongoose.model( "QuestionComplexityMaster" );

const getQuestionList = ( data ) => {
    let queryparam = {};;
    if(data) {
        queryparam = {
            'questionId': { $in: data}
        };
    }

    return QuestionMaster.find(queryparam);
};

const addQuestion = ( data ) => {
    // const questionMaster = new QuestionMaster( data );
    return QuestionMaster.create(data);
};

const editQuestion = ( data ) => {
    const query = {questionId: data.questionId};
  return QuestionMaster.findOneAndUpdate(query, {$set: data}, {new: true});
};

const deleteQuestion = ( data ) => {
    return QuestionMaster.findOneAndRemove({'questionId': data.questionId});
}

const getTechnologyList = ( ) => {
    return TechnologyMaster.find();
};
const getQuestionComplexity = ( ) => {
    return QuestionComplexityMaster.find();
};

module.exports = {
    getQuestionList,
    addQuestion,
    getTechnologyList,
    getQuestionComplexity,
    editQuestion,
    deleteQuestion
};
