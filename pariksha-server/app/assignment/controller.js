const services = require( "./services" );
const candidateService = require('../candidate_detail/services')

exports.getQuestionList = async ( req, res ) => {
    try {
        const userDetail = await candidateService.getCandidateDetail( req.query );
        const questionList = await services.getQuestionList( userDetail[0] );
        const testQuestion = generateQuestionlist(userDetail[0], questionList);
        res.success( testQuestion );
    } catch ( err ) {
        res.send( err );
    }
};

exports.submitTest = async ( req, res ) => {
    try {
        const getResult = await services.submitTest( req.body );
        res.success( getResult );
    } catch ( err ) {
        res.send( err );
    }
};

// Math.random() * 30
function generateQuestionlist(userDetail, allQuestion) {
    const primaryskill = userDetail.technologies.primaryskill[0];
    const secondaryskill = userDetail.technologies.secondaryskill[0];

    // const questionByTechnology = allQuestion.groupBy('technologyId');

    const testQuestion = {
        candidateId: userDetail.candidateId,
        primaryskill: primaryskill,
        secondaryskill: secondaryskill,
        questionList: allQuestion.slice(0, 30)
    }
    return testQuestion;
}