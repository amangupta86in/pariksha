const mongoose = require( "mongoose" );
const candidateService = require( "../candidate_detail/services" );
const questionFilter = require('../../constants/constants');

const TestDetails = mongoose.model( "TestDetails" );
const QuestionMaster = mongoose.model( "QuestionMaster" );

const getQuestionList = ( userDetail ) => {
    const query = setQuestionListWhereClause(userDetail);
    return QuestionMaster.find({ $or: query });
};

const submitTest = async ( data ) => {
    const testDetails = new TestDetails( data );

    const totalScore = { score: data.totalMarks, candidateId: data.candidateId };
    const scoreUpdated = await candidateService.updateCandidate(totalScore);
    
    return testDetails.save();
};

function setQuestionListWhereClause(userDetail) {
    
    const primaryskill = userDetail.technologies.primaryskill;
    const secondaryskill = userDetail.technologies.secondaryskill;

    const primeryComp = questionFilter[userDetail.experience].primary;
    const secondaryComp = questionFilter[userDetail.experience].secondary;

    const query = [];

    if(primaryskill.length) {
        query.push({'technologyId': { '$in' : primaryskill}, 'complexity':  { $lte: primeryComp }},);
    }

    if(secondaryskill.length) {
        query.push({'technologyId': { '$in' : secondaryskill},'complexity':  { $lte: secondaryComp }},);
    }
    return query;
}

module.exports = {
    getQuestionList,
    submitTest,
};


