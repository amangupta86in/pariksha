const mongoose = require('mongoose');

const UserMaster = mongoose.model( "UserMaster" );
const UserRole = mongoose.model( "UserRole" );
const CandidateDetail = mongoose.model( "CandidateDetail" );


const getUserList = ( ) => {
    return UserMaster.find().limit(30);
};

const addUser = ( data ) => {
    const userMaster = new UserMaster( data );
    return userMaster.save();
};

const updateUser = (data) => {
  const query = {userId: data.userId};
  return UserMaster.findOneAndUpdate(query, {$set: data}, {new: true});
};

const getUserRole = () => {
  return UserRole.find();
};

const getUserByEmail = (email) => {
  return UserMaster.findOne({email});
};

const getCandidateEmail = (email) => {
    return CandidateDetail.findOne({email});
};

module.exports = {
  getUserList,
  addUser,
  getUserRole,
  updateUser,
  getUserByEmail,
  getCandidateEmail
};
