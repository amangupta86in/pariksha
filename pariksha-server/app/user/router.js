require( "../../models/user_model" );
const controller = require( "./controller" );
const express = require( "express" );
const router = express.Router( );

router.get('/secure/getUserList', controller.getUserList);
router.post('/secure/addUser', controller.addUser);
router.post('/secure/updateUser', controller.updateUser);
router.get('/secure/getUserRole', controller.getUserRole);
router.post('/login',controller.login);

module.exports = router;
