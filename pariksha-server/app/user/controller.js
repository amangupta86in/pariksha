const services = require('./services');
const jwt = require('jsonwebtoken');
const config = require('../../config');


exports.getUserList = async (req, res) => {
  try {
    const getResult = await services.getUserList();
    res.success(getResult);
  } catch (err) {
    res.send(err);
  }
};

exports.addUser = async (req, res) => {
  try {
    const getResult = await services.addUser(req.body);
    res.success(getResult);
  } catch (err) {
    res.send(err);
  }
};

exports.updateUser = async (req, res) => {
  try {
    const getResult = await services.updateUser(req.body);
    res.success(getResult);
  } catch (err) {
    res.send(err);
  }
};

exports.getUserRole = async (req, res) => {
  try {
    const getResult = await services.getUserRole(req.body);
    res.success(getResult);
  } catch (err) {
    res.send(err);
  }
};

/**
 * This Api will authenticate user against its email and password.
 * If authentication is successfull, it returns a authentication token as a
 * response.
 */
exports.login = async (req, res) => {
  const {email, password} = req.body;
  const user = await services.getUserByEmail(email);
  const candidate = await services.getCandidateEmail(email);

  if (user && user.password == password) {
    // User is authenticated successfully
    const token = jwt.sign({email, role: user.role}, config.TOKEN_SECRET, {
      expiresIn: 86400  // expires in 24 hours
    });
    res.success({
      userId: user.userId,
      firstName: user.firstName,
      lastName: user.lastName,
      role: user.role,
      email: email,
      token
    });
  } else if (candidate && candidate.mobileNo == password) {
    // Candidate is authenticated successfully
    const token = jwt.sign({email, role: candidate.profile}, config.TOKEN_SECRET, {
      expiresIn: 86400  // expires in 24 hours
    });
    res.success({
      candidateId: candidate.candidateId,
      firstName: candidate.firstName,
      lastName: candidate.lastName,
      email: email,
      role: candidate.role,
      token
    });
  } else {
    
    //Authentication failed
    res.send({msg: 'AUTH FAILED'})
  }
}
